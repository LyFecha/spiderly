To execute the code, you need to create a .env file in the same folder as the README with the private token of your discord bot. The .env should look like this :

```
DISCORD_BOT_TOKEN=<your-bot-token>
TEST_SERVER_ID=<your debug server id (optional)>
CHROMIUM_PATH=<absolute path to your chromium installation>
```

To run, you need node with npm or yarn and run the following command in the folder:

```
npm i
```

then, either :

```
npm run start
```

or with the nodemon package installed :

```
npm run dev
```
