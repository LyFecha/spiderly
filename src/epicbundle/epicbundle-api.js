const { getPuppeteer, killBrowser } = require("../utils");

const puppeteer = getPuppeteer();
// const StealthPlugin = require("puppeteer-extra-plugin-stealth");
// puppeteer.use(StealthPlugin());

let lastFreeGame;

const main = async (env = {}) => {
  console.log("[EPIC] Démarrage du navigateur");
  const browser = await puppeteer.launch({
    timeout: 60000,
    headless: true,
    executablePath: env.CHROMIUM_PATH,
    waitForInitialPage: false,
    args:
      env.ON_SERVER !== "true"
        ? []
        : [
            "--headless",
            "--aggressive-cache-discard",
            "--disable-cache",
            "--disable-application-cache",
            "--disable-offline-load-stale-cache",
            "--disable-gpu-shader-disk-cache",
            "--media-cache-size=0",
            "--disk-cache-size=0",
            "--no-sandbox",
            "--no-zygote",
            "--single-process", // <- this one doesn't works in Windows
          ],
  });
  const page = await browser.newPage();
  await page.setCacheEnabled(false);
  await page.setUserAgent(
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36"
  );

  let freeGames = [];

  try {
    console.log("[EPIC] Goto epicbundle...");
    await page
      .goto("https://www.epicbundle.com/category/article/for-free/", {
        waitUntil: "networkidle2",
      })
      .catch((e) =>
        console.log(
          "[EPIC][ERROR] Une minute s'est écoulée et la page n'est toujours pas affiché :/",
          e
        )
      );
    console.log("[EPIC] Epicbundle chargé !");

    if (!lastFreeGame) {
      lastFreeGame = await page.$("article", (elem) => elem.id);
      // console.log("[EPIC] Id du dernier article sur un jeu gratuit", lastFreeGame);
      console.log(
        "[EPIC] Initialisation du dernier article sur un jeu gratuit"
      );
      return [];
    }

    const articles = await page.$$("article");
    // console.log("[EPIC] Articles récupérés");

    const indexLastFreeGame = (
      await Promise.all(
        articles.map(
          async (article) => await article.evaluate((elem) => elem.id)
        )
      )
    ).findIndex((articleId) => articleId === lastFreeGame);

    const newArticles = articles.slice(0, indexLastFreeGame);

    const articlesLinks = await Promise.all(
      newArticles.map(async (newArticle) =>
        newArticle.$eval("div > a", (elem) => elem.href)
      )
    );

    freeGames = await articlesLinks.reduce(async (linkAccPromise, link) => {
      const linkAcc = await linkAccPromise;
      // console.log("[EPIC] Goto page epicbundle :");
      // console.log(link);
      const articlePage = await browser.newPage();
      await articlePage
        .goto(link, {
          waitUntil: "networkidle2",
        })
        .catch((e) =>
          console.log(
            "[EPIC][ERREUR] La page de l'article a mis trop de temps à se charger",
            e
          )
        );
      const freeGameLinks = await articlePage.$eval(
        "div.entry-content.clearfix > p > a",
        (elem) => elem.href
      );
      // console.log("Fermeture de la page epicbundle");
      await articlePage.close();
      // console.log("Lien trouvé : ", freeGameLinks);
      linkAcc.push(freeGameLinks);
      return linkAcc;
    }, []);

    lastFreeGame = await articles[0].evaluate((elem) => elem.id);
  } catch (error) {
    console.log("[EPIC][ERREUR] ", error.stack);
  } finally {
    const process = browser.process();
    console.log("[EPIC] Fermeture du navigateur et envoie des liens");
    await browser.close();
    console.log("[EPIC] Kill des processus");
    if (env.ON_SERVER === "true") killBrowser(process);
    return freeGames.filter(
      (link) =>
        !(
          link.includes("indiegala") ||
          link.includes("amazon") ||
          link.includes("playstation")
        )
    );
  }
};

module.exports = main;
