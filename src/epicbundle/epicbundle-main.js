const EpicBundleAPI = require("./epicbundle-api");
const EpicBundleDAO = require("./epicbundle-dao");
const PREF = "$";

const onMessage = async (message) => {
  if (message.channel.type === "dm") {
    if (
      message.author.tag === "Ly Fecha#6559" &&
      message.content.toLowerCase().match(/\$addebserver\s\d*/)
    ) {
      const serverId = message.content.split(/\s+/)[1];
      console.log(`[EPIC] Ajout du serveur : `, serverId);
      EpicBundleDAO.addServer(serverId);
      message.author.send("Bien reçu !");
    }
    console.log(`[EPIC] Appel en DM`);
    return true;
  }

  const server = await EpicBundleDAO.getServer(message.guild.id);

  if (server && message.content.startsWith(PREF)) {
    const ctn = message.content.slice(1).toLowerCase();
    if (ctn.match(/^epicbu?n?d?l?e?ch?a?n?n?e?l?\s*/)) {
      EpicBundleDAO.addChannelServer(server, message.channel);
      message.channel.send(
        "Ce channel contiendra désormais les alertes de jeux gratuits grâce à Epic Bundle"
      );
    } else {
      return false;
    }
  }
  return true;
};

const job = async (client) => {
  const newFreeGames = await EpicBundleAPI(process.env);
  EpicBundleDAO.getAllServers().then((servers) =>
    servers.forEach(
      (server) =>
        server.channel_id &&
        client.channels.fetch(server.channel_id).then((channel) => {
          if (
            process.env.TEST_SERVER_ID &&
            !(process.env.TEST_SERVER_ID == server.id)
          )
            return;
          newFreeGames.forEach((newFreeGame) => {
            channel.send("Il y a un nouveau jeu gratuit !\n" + newFreeGame);
          });
        })
    )
  );
};

module.exports = { onMessage, job };
