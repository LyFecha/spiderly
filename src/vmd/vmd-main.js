const Utils = require("../utils.js");
const VMDDAO = require("./vmd-dao.js");
const VMDAPI = require("./vmd-api.js");

const CMD = {
  HELP: /^he?l?p?\s*$/,
  SET_CHANNEL: /^setcha?n?n?e?l?\s*$/,
  GET_CENTRE: /^getce?n?t?r?e?d?e?p?a?r?t?e?m?e?n?t?\s+\d\d?\s*$/,
  SUB_DEPARTEMENT: /^subde?p?a?r?t?e?m?e?n?t?\s+\d\d?\s*$/,
  SUB_VILLE: /^subvi?l?l?e?\s+\d\d?\s+[^,]+(\s*,\s*[^,]*)*\s*$/,
  UNSUB: /^unsub\s*$/,
  UNAVAILABLE:
    /^pasdi?s?p?o?n?i?b?l?e?\s+\d\d?([:hH]\d\d?)?-\d\d?([:hH]\d\d?)?(\s*,\s*\d\d?([:hH]\d\d?)?-\d\d?([:hH]\d\d?)?)*\s*$/,
  RESET_DISPONIBLE: /^re?se?tdi?s?p?o?n?i?b?l?e?\s*$/,
  DEBUG: /^debug\s*$/,
};

const PREF = "$";

const notAvailableTimes = VMDDAO.getAllUnavailable();

const onMessage = async (message) => {
  if (message.channel.type === "dm") {
    if (
      message.author.tag === "Ly Fecha#6559" &&
      message.content.toLowerCase().match(/\$addserver\s\d*/)
    ) {
      const serverId = message.content.split(/\s+/)[1];
      console.log(`Ajout du serveur : `, serverId);
      VMDDAO.addServer(serverId);
      message.author.send("Bien reçu !");
    }
    console.log(`Appel en DM`);
    return true;
  }

  const server = await VMDDAO.getServer(message.guild.id);

  if (server && message.content.startsWith(PREF)) {
    const ctn = message.content.slice(1).toLowerCase();
    if (ctn.match(CMD.HELP)) {
      message.channel.send(
        "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-\n" +
          "`$help` **Affiche les commandes**\n" +
          " --> *alias :* `$h`\n" +
          "`$setChannel` **Envoie les alertes sur ce channel**\n" +
          " --> *alias :* `$setCh`\n" +
          "`$getCentreDepartement 00` **Affiche tous les centres du département 00.**\n" +
          " --> *alias :* `$getC 00`\n" +
          "`$subDepartement 00` **Abonne aux alertes de chronodose sur le departement 00**\n" +
          " --> *alias :* `$subDpt 00`\n" +
          "`$subVille 00 ville1[, ville2, ...]` **Abonne aux alertes de chronodoses pour les villes ville1, ville2, ...**\n" +
          " --> *alias :* `$subV 00 ville1[, ville2, ...]`\n" +
          "`$pasDisponible HH:MM-HH:MM[, HH:MM-HH:MM, ...]` **Spécifie les plages horaires des chronodoses qui ne vous intéressent pas (ne filtre pas les RDV du week-end)**\n" +
          " --> *alias :* `$pasD HH:MM-HH:MM[, HH:MM-HH:MM, ...]`\n" +
          "`$resetDisponible` **Supprime les plages horaires spécifiées par $pasDisponible**\n" +
          " --> *alias :* `$rstD`\n" +
          "`$unSub` **Desabonne aux alertes de chronodoses (villes et départements)**\n" +
          "\n" +
          "`00` est à remplacer par le code du departement\n" +
          "`ville1[, ville2, ...]` est à remplacer par des noms de villes (au moins une) séparés par des virgules\n" +
          "`HH:MM-HH:MM[, HH:MM-HH:MM, ...]` est à remplacer par des intervalles heures:minutes (les minutes sont facultatifs)\n" +
          "La casse (majuscule/minuscule) n'est pas pris en compte pour les commandes\n" +
          "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-"
      );
    } else if (ctn.match(CMD.SET_CHANNEL)) {
      VMDDAO.addChannelServer(server, message.channel);
      message.channel.send(
        "Ce channel contiendra désormais les alertes de chronodoses."
      );
    } else if (ctn.match(CMD.GET_CENTRE)) {
      const codeDepartement = ctn.split(/\s+/)[1];
      message.channel.send(
        `Voici tous les centres que j'ai trouvé :\n\n${(
          await VMDAPI.getDepartement(
            server,
            codeDepartement.length === 2
              ? codeDepartement
              : "0" + codeDepartement
          )
        )
          .map((centre) => `${centre.city}  -  *${centre.nom}*`)
          .join("\n")}`
      );
    } else if (ctn.match(CMD.SUB_DEPARTEMENT)) {
      const codeDepartement = ctn.split(/\s+/)[1];
      VMDDAO.addSubDepartement(server, message.author, codeDepartement);
      message.channel.send(
        `${message.author}, tu as bien été abonné·e au département ayant pour code : ${codeDepartement}`
      );
    } else if (ctn.match(CMD.SUB_VILLE)) {
      const codeDepartement = ctn.split(/\s+/)[1];
      const villes = ctn
        .split(/\s+/)
        .slice(2)
        .join(" ")
        .split(/\s*,\s*/);
      VMDDAO.addSubVilles(server, message.author, codeDepartement, villes);
      message.channel.send(
        `${message.author}, tu as bien été abonné·e aux villes saisies du département ayant pour code : ${codeDepartement}`
      );
    } else if (ctn.match(CMD.UNSUB)) {
      VMDDAO.deleteSub(server, message.author);
      message.channel.send(`${message.author}, tu as bien été desabonné·e.`);
    } else if (ctn.match(CMD.UNAVAILABLE)) {
      const timeIntervals = ctn
        .split(/\s+/)
        .slice(1)
        .join(" ")
        .split(/\s*,\s*/)
        .map((interval) => interval.split("-"));
      if (!notAvailableTimes[server.id]) notAvailableTimes[server.id] = {};
      if (!notAvailableTimes[server.id][message.author.id])
        notAvailableTimes[server.id][message.author.id] = [];
      notAvailableTimes[server.id][message.author.id].splice(
        0,
        0,
        ...timeIntervals
      );

      VMDDAO.addUnavailable({
        [`${server.id}`]: { [`${message.author.id}`]: timeIntervals },
      });
      message.channel.send(
        `${
          message.author
        }, les alertes ont bien été désactivé sur les créneaux : \n\n${notAvailableTimes[
          server.id
        ][message.author.id]
          .map((int) => `De ${int[0]} à ${int[1]}`)
          .join("\n")}`
      );
    } else if (ctn.match(CMD.RESET_DISPONIBLE)) {
      if (
        notAvailableTimes &&
        notAvailableTimes[server.id] &&
        notAvailableTimes[server.id][message.author.id]
      )
        notAvailableTimes[server.id][message.author.id] = [];
      message.channel.send(
        `${message.author}, tes créneaux de disponibilité ont bien été réinitialisés.`
      );
    } else if (ctn.match(CMD.DEBUG) && message.author.tag === "Ly Fecha#6559") {
      await VMDDAO.debug();
      VMDAPI.resetPastRDV();
    } else {
      return false;
    }
  }

  return true;
};

const job = (client) => {
  VMDDAO.getAllServers()
    .then((servers) =>
      servers.forEach(
        (server) =>
          server.channel_id &&
          client.channels
            .fetch(server.channel_id)
            .then(async (channel) => {
              if (
                process.env.TEST_SERVER_ID &&
                !(process.env.TEST_SERVER_ID == server.id)
              )
                return;
              const subsServer = await VMDDAO.findSubByServer(server);
              const chronodoses =
                await VMDAPI.getNouvellesChronodosesDepartements(server, [
                  ...new Set(subsServer.map((sub) => sub.code_departement)),
                ]);
              chronodoses.forEach((chronodose) => {
                const rdv = chronodose.prochain_rdv.tz("Europe/Paris");
                const ats = subsServer
                  .filter(
                    (sub) =>
                      chronodose.departement === sub.code_departement &&
                      (!sub.ville ||
                        Utils.normalize(sub.ville) ===
                          Utils.normalize(chronodose.city)) &&
                      (rdv.isoWeekday() >= 6 ||
                        !notAvailableTimes[server.id] ||
                        !notAvailableTimes[server.id][sub.id_user] ||
                        notAvailableTimes[server.id][sub.id_user].every(
                          (interval) => {
                            const [heureD, minutesD] =
                              interval[0].split(/[:hH]/);
                            const [heureF, minutesF] =
                              interval[1].split(/[:hH]/);
                            const deb = moment(rdv)
                              .hour(parseInt(heureD))
                              .minute(minutesD ? parseInt(minutesD) : 0);
                            const fin = moment(rdv)
                              .hour(parseInt(heureF))
                              .minute(minutesF ? parseInt(minutesF) : 0);
                            return rdv.isBefore(deb) || rdv.isAfter(fin);
                          }
                        ))
                  )
                  .map((sub) => sub.at);
                if (!ats.length) return;
                channel.send(
                  `${ats.join(" ")}, une chronodose est disponible ici !\n${
                    chronodose.url
                  }\n***${chronodose.nom}***\n${chronodose.address}\n${
                    chronodose.departement
                  } - *${chronodose.city}*\n${
                    chronodose.chronodoses
                  } chronodoses restantes à partir du : **${rdv.format(
                    "dddd D MMMM [à] H:mm"
                  )}**\nSur *${chronodose.plateforme}*`
                );
              });
            })
            .catch((err) => console.log(`[ERREUR] : ${err}`))
      )
    )
    .catch((err) => console.log(`[ERREUR] : ${err}`));
};

module.exports = { onMessage, job };
