const fetch = require("node-fetch");
const moment = require("moment");
moment.locale("fr");

let pastRDVs = {};

const garbageCollectionRDV = (server) => {
  const pastRDVServer = pastRDVs[server.id];
  if (!pastRDVServer) return;
  for (let i = 0; i < pastRDVServer.length; i++) {
    let rdv = pastRDVServer[i];
    const today = moment();

    if (rdv.prochain_rdv.isBefore(today)) {
      pastRDVServer.splice(i, 1);
      i--;
    }
  }
};

const isPastRDV = (server, rdv) => {
  return (
    pastRDVs[server.id] &&
    pastRDVs[server.id].find(
      (pastRDV) =>
        pastRDV.internal_id === rdv.internal_id &&
        pastRDV.prochain_rdv.isSame(rdv.prochain_rdv)
    )
  );
};

const resetPastRDV = () => {
  pastRDVs = {};
};

const addToPastRDV = (server, rdv) => {
  if (!pastRDVs[server.id]) pastRDVs[server.id] = [];
  else if (!isPastRDV(server, rdv)) {
    pastRDVs[server.id].push(rdv);
  }
};

const getDepartement = async (server, code) => {
  const json = await fetch(
    `https://vitemadose.gitlab.io/vitemadose/${code}.json`
  )
    .then((res) => res.json())
    .catch((err) => console.log(`[ERREUR] : ${err}`))
    .then((json) => json.centres_disponibles)
    .catch((err) => console.log(`[ERREUR] : ${err}`));
  garbageCollectionRDV(server);
  return json.map((centre) => {
    const { internal_id, nom, url, plateforme, departement, vaccine_type } =
      centre;
    const prochain_rdv = moment.utc(centre.prochain_rdv);
    const chronodoses = centre.appointment_count;
    return {
      internal_id,
      nom,
      url,
      plateforme,
      prochain_rdv,
      departement,
      city: centre.location.city,
      address: centre.metadata.address,
      vaccine_type,
      chronodoses,
    };
  });
};

const getNouvellesChronodosesDepartements = async (server, codes) => {
  return (
    await Promise.all(
      codes.map(async (code) => await getDepartement(server, code))
    )
  )
    .reduce((acc, val) => acc.concat(val), [])
    .filter((rdv) => !isPastRDV(server, rdv) && rdv.chronodoses)
    .map((rdv) => {
      addToPastRDV(server, rdv);
      return rdv;
    });
};

module.exports = {
  getDepartement,
  getNouvellesChronodosesDepartements,
  resetPastRDV,
};
