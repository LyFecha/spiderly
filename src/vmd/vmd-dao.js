const getDB = require("../init-db.js");

const db = getDB();

const setUser = (author) => {
  return new Promise((resolve, reject) =>
    db.get("SELECT * FROM user WHERE id = ?", [author.id], (error, row) => {
      if (error) {
        console.log(error);
        return reject(error);
      }
      if (!row) {
        db.run("INSERT INTO user (id, at) VALUES (?, ?)", [author.id, author]);
        resolve({ id: author.id, at: author });
      }
      resolve(row);
    })
  );
};

const getServer = (serverId) => {
  return new Promise((resolve, reject) =>
    db.get("SELECT * FROM server WHERE id = ?", [serverId], (error, row) => {
      if (error) {
        console.log(error);
        return reject(error);
      }
      return resolve(row);
    })
  );
};

const getAllServers = () => {
  return new Promise((resolve, reject) =>
    db.all("SELECT * FROM server", [], (error, rows) => {
      if (error) {
        console.log(error);
        return reject(error);
      }
      return resolve(rows);
    })
  );
};

const addServer = (serverId) => {
  db.run(
    "INSERT INTO server (id) SELECT ? WHERE NOT EXISTS (SELECT 1 FROM server WHERE id = ?)",
    [serverId, serverId],
    (error) => {
      if (error) {
        console.log(error);
        return error;
      }
    }
  );
};

const addChannelServer = (server, channel) => {
  db.run(
    "UPDATE server SET channel_id = ? WHERE id = ?",
    [channel.id, server.id],
    (error) => {
      if (error) {
        console.log(error);
        return error;
      }
    }
  );
};

const findSubByServer = (server) => {
  return new Promise((resolve, reject) =>
    db.all(
      "SELECT * FROM sub_vitemadose JOIN user ON user.id = sub_vitemadose.id_user WHERE id_server = ?",
      [server.id],
      (error, rows) => {
        if (error) {
          console.log(error);
          return reject(error);
        }
        return resolve(rows);
      }
    )
  );
};

const addSubDepartement = (server, author, codeDepartement) => {
  setUser(author);
  const finalCodeDepartement =
    codeDepartement.length === 2 ? codeDepartement : "0" + codeDepartement;
  db.run(
    "INSERT INTO sub_vitemadose (id_user, id_server, code_departement) SELECT ?, ?, ? WHERE NOT EXISTS (SELECT 1 FROM sub_vitemadose WHERE id_user = ? AND id_server = ? AND code_departement = ?)",
    [
      author.id,
      server.id,
      finalCodeDepartement,
      author.id,
      server.id,
      finalCodeDepartement,
    ],
    (error) => {
      if (error) {
        console.log(error);
        return error;
      }
    }
  );
};

const addSubVilles = (server, author, codeDepartement, villes) => {
  setUser(author);
  const finalCodeDepartement =
    codeDepartement.length === 2 ? codeDepartement : "0" + codeDepartement;
  db.serialize(() => {
    for (ville of villes) {
      db.run(
        "INSERT INTO sub_vitemadose (id_user, id_server, code_departement, ville) SELECT ?, ?, ?, ? WHERE NOT EXISTS (SELECT 1 FROM sub_vitemadose WHERE id_user = ? AND id_server = ? AND code_departement = ? AND (ville = NULL OR ville = ?))",
        [
          author.id,
          server.id,
          finalCodeDepartement,
          ville,
          author.id,
          server.id,
          finalCodeDepartement,
          ville,
        ],
        (error) => {
          if (error) {
            console.log(error);
            return error;
          }
        }
      );
    }
  });
};

const getAllUnavailable = () => {
  return new Promise((resolve, reject) =>
    db.all("SELECT * FROM unavailable_vitemadose", [], (error, rows) => {
      if (error) {
        console.log(error);
        return reject(error);
      }
      const notAvailableTimes = {};
      rows.forEach((row) => {
        if (!notAvailableTimes[row.server_id])
          notAvailableTimes[row.server_id] = {};
        if (!notAvailableTimes[row.server_id][row.user_id])
          notAvailableTimes[row.server_id][row.user_id] = [];
        notAvailableTimes[row.server_id][row.user_id].splice(0, 0, [
          row.start,
          row.end,
        ]);
      });
      return resolve(notAvailableTimes);
    })
  );
};

const addUnavailable = (notAvailableTimes) => {
  db.serialize(() => {
    Object.keys(notAvailableTimes).forEach((serverId) =>
      Object.keys(notAvailableTimes[serverId]).forEach((authorId) =>
        notAvailableTimes[serverId][authorId].forEach((interval) =>
          db.run(
            "INSERT INTO unavailable_vitemadose (id_user, id_server, start, end) SELECT ?, ?, ?, ? WHERE NOT EXISTS (SELECT 1 FROM unavailable_vitemadose WHERE id_user = ? AND id_server = ? AND start = ? AND end = ?)",
            [
              authorId,
              serverId,
              interval.start,
              interval.end,
              authorId,
              serverId,
              interval.start,
              interval.end,
            ],
            (error) => {
              if (error) {
                console.log(error);
                return error;
              }
            }
          )
        )
      )
    );
  });
};

const deleteSub = (server, author) => {
  db.serialize(() => {
    db.run("DELETE FROM sub_vitemadose WHERE id_user = ? AND id_server = ?", [
      author.id,
      server.id,
    ]);
    db.run("DELETE FROM user WHERE id = ?", [author.id]);
  });
};

const debug = () => {
  db.serialize(() => {
    db.all("SELECT * FROM server", [], (_, rows) => console.log(rows));
    db.all("SELECT * FROM user", [], (_, rows) => console.log(rows));
    db.all("SELECT * FROM sub_vitemadose", [], (_, rows) => console.log(rows));
  });
};

module.exports = {
  setUser,
  getServer,
  getAllServers,
  addServer,
  addChannelServer,
  findSubByServer,
  addSubDepartement,
  addSubVilles,
  deleteSub,
  getAllUnavailable,
  addUnavailable,
  debug,
};
