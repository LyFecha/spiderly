CREATE TABLE IF NOT EXISTS user (id INT NOT NULL, at TEXT, PRIMARY KEY (id));
CREATE TABLE IF NOT EXISTS sub_vitemadose (id INT NOT NULL, id_user INT NOT NULL, id_server INT NOT NULL, code_departement TEXT, ville TEXT, PRIMARY KEY (id), FOREIGN KEY (id_server) REFERENCES server(id), FOREIGN KEY (id_server) REFERENCES server(id));
CREATE TABLE IF NOT EXISTS server (id INT NOT NULL, channel_id TEXT, discord_guild_id TEXT, PRIMARY KEY (id));
INSERT INTO server (id) SELECT (?) WHERE NOT EXISTS (SELECT 1 FROM server WHERE id = ?);