const { getPuppeteer, killBrowser } = require("../utils");

const puppeteer = getPuppeteer();
// const StealthPlugin = require("puppeteer-extra-plugin-stealth");
// puppeteer.use(StealthPlugin());

const main = async (env = {}) => {
  console.log("[QWERTEE] Démarrage du navigateur");
  const browser = await puppeteer.launch({
    timeout: 60000,
    headless: true,
    executablePath: env.CHROMIUM_PATH,
    waitForInitialPage: false,
    args:
      env.ON_SERVER !== "true"
        ? []
        : [
            "--headless",
            "--aggressive-cache-discard",
            "--disable-cache",
            "--disable-application-cache",
            "--disable-offline-load-stale-cache",
            "--disable-gpu-shader-disk-cache",
            "--media-cache-size=0",
            "--disk-cache-size=0",
            "--no-sandbox",
            "--no-zygote",
            "--single-process", // <- this one doesn't works in Windows
          ],
  });
  const page = await browser.newPage();
  await page.setCacheEnabled(false);
  await page.setUserAgent(
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/47.0.2526.111 Safari/537.36"
  );

  const shirts = {};

  try {
    console.log("[QWERTEE] Goto Qwertee...");
    await page
      .goto("https://www.qwertee.com/", { waitUntil: "networkidle2" })
      .catch(() =>
        console.log(
          "[QWERTEE][ERREUR] Une minute s'est écoulée et la page n'est toujours pas affiché :/"
        )
      );
    console.log("[QWERTEE] Qwertee chargé !");

    await page.$$eval("a.arrow.next", (elems) =>
      elems.slice(0, 3).map(async (elem) => await elem.click())
    );

    const titles = await page.$$eval("div.title > div > span", (elems) =>
      elems.map((elem) => elem.innerText)
    );
    const imgs = await page.$$eval(
      "div.buy > div.design-dynamic-image-wrap > div.zoom-dynamic-image.design-dynamic-image > picture > img",
      (elems) => elems.map((elem) => elem.src)
    );

    [0, 1, 2].forEach(async (ind) => (shirts[titles[ind]] = imgs[ind]));
  } catch (error) {
    console.log("[QWERTEE][ERREUR] ", error.stack);
  } finally {
    const process = browser.process();
    console.log("[QWERTEE] Fermeture du navigateur et envoie des liens");
    await browser.close();
    console.log("[QWERTEE] Kill des processus");
    if (env.ON_SERVER === "true") killBrowser(process);
    return shirts;
  }
};

module.exports = main;
