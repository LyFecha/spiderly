const QwerteeAPI = require("./qwertee-api");
const QwerteeDAO = require("./qwertee-dao");
const PREF = "$";

const onMessage = async (message) => {
  if (message.channel.type === "dm") {
    if (
      message.author.tag === "Ly Fecha#6559" &&
      message.content.toLowerCase().match(/\$addqwerteeserver\s\d*/)
    ) {
      const serverId = message.content.split(/\s+/)[1];
      console.log("[QWERTEE] Ajout du serveur : ", serverId);
      QwerteeDAO.addServer(serverId);
      message.author.send("Bien reçu !");
    }
    console.log("[QWERTEE] Appel en DM");
    return true;
  }

  const server = await QwerteeDAO.getServer(message.guild.id);

  if (server && message.content.startsWith(PREF)) {
    const ctn = message.content.slice(1).toLowerCase();
    if (ctn.match(/^qwerteech?a?n?n?e?l?\s*/)) {
      QwerteeDAO.addChannelServer(server, message.channel);
      message.channel.send(
        "Ce channel contiendra désormais les nouveaux t-shirts sur Qwertee.com."
      );
    } else if (ctn.match(/^redo\s*/)) {
      console.log("[QWERTEE] Execution manuelle du scraping de Qwertee");
      job(message.client);
    } else {
      return false;
    }
  }
  return true;
};

const job = async (client) => {
  console.log("[QWERTEE] Scraping de Qwertee");
  const shirts = await QwerteeAPI(process.env);
  QwerteeDAO.getAllServers().then((servers) =>
    servers.forEach(
      (server) =>
        server.channel_id &&
        client.channels.fetch(server.channel_id).then((channel) => {
          if (
            process.env.TEST_SERVER_ID &&
            !(process.env.TEST_SERVER_ID == server.id)
          )
            return;
          channel.send("Voici les designs de t-shirts du jour :");
          Object.getOwnPropertyNames(shirts).forEach((shirtKey) =>
            channel.send(`${shirtKey} :\n${shirts[shirtKey]}`)
          );
        })
    )
  );
};

module.exports = { onMessage, job };
