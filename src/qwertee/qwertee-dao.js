const getDB = require("../init-db.js");

const db = getDB();

const getServer = (serverId) => {
  return new Promise((resolve, reject) =>
    db.get(
      "SELECT * FROM qwertee_server WHERE id = ?",
      [serverId],
      (error, row) => {
        if (error) {
          console.log(error);
          return reject(error);
        }
        return resolve(row);
      }
    )
  );
};

const getAllServers = () => {
  return new Promise((resolve, reject) =>
    db.all("SELECT * FROM qwertee_server", [], (error, rows) => {
      if (error) {
        console.log(error);
        return reject(error);
      }
      return resolve(rows);
    })
  );
};

const addChannelServer = (server, channel) => {
  db.run(
    "UPDATE qwertee_server SET channel_id = ? WHERE id = ?",
    [channel.id, server.id],
    (error) => {
      if (error) {
        console.log(error);
        return error;
      }
    }
  );
};

const addServer = (serverId) => {
  db.run(
    "INSERT INTO qwertee_server (id) SELECT ? WHERE NOT EXISTS (SELECT 1 FROM qwertee_server WHERE id = ?)",
    [serverId, serverId],
    (error) => {
      if (error) {
        console.log(error);
        return error;
      }
    }
  );
};

module.exports = { getServer, getAllServers, addChannelServer, addServer };
