const sqlite = require("sqlite3").verbose();

let db = undefined;

const initializeDB = () => {
  const newDB = new sqlite.Database("botdiscord.sqlite");
  newDB.serialize(() => {
    newDB.run("CREATE TABLE IF NOT EXISTS user (id TEXT PRIMARY KEY, at TEXT)");
    newDB.run(
      "CREATE TABLE IF NOT EXISTS sub_vitemadose (id_user TEXT NOT NULL, id_server TEXT NOT NULL, code_departement TEXT, ville TEXT, FOREIGN KEY (id_user) REFERENCES user(id), FOREIGN KEY (id_server) REFERENCES server(id))"
    );
    newDB.run(
      "CREATE TABLE IF NOT EXISTS server (id TEXT PRIMARY KEY, channel_id TEXT)"
    );
    newDB.run(
      "CREATE TABLE IF NOT EXISTS unavailable_vitemadose (id_user TEXT NOT NULL, id_server TEXT NOT NULL, start TEXT, end TEXT, FOREIGN KEY (id_user) REFERENCES user(id), FOREIGN KEY (id_server) REFERENCES server(id))"
    );
    newDB.run(
      "CREATE TABLE IF NOT EXISTS qwertee_server (id TEXT PRIMARY KEY, channel_id TEXT)"
    );
    newDB.run(
      "CREATE TABLE IF NOT EXISTS epicbundle_server (id TEXT PRIMARY KEY, channel_id TEXT)"
    );
  });
  db = newDB;
  return newDB;
};

const getDB = () => {
  return db || initializeDB();
};

module.exports = getDB;
