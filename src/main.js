require("dotenv").config();

const { Client } = require("discord.js");
const schedule = require("node-schedule");
const moment = require("moment-timezone");
moment.locale("fr");

const VMDMain = require("./vmd/vmd-main.js");
const QwerteeMain = require("./qwertee/qwertee-main.js");
const EpicBundleMain = require("./epicbundle/epicbundle-main.js");
const client = new Client();

client.on("ready", () => {
  console.log("[COOLNESS] On est cool !");
});

client.on("message", async (message) => {
  // Ligne de debug !
  if (
    process.env.TEST_SERVER_ID &&
    message.guild &&
    process.env.TEST_SERVER_ID !== message.guild.id
  )
    return;
  if (message.author.bot) return;

  const commandVMDFound = await VMDMain.onMessage(message);
  const commandQwerteeFound = await QwerteeMain.onMessage(message);
  const commandEBFound = await EpicBundleMain.onMessage(message);

  if (!commandVMDFound && !commandQwerteeFound && !commandEBFound) {
    message.channel.send(`${message.author}, mauvaise commande ou paramétrage`);
  }

  if (message.guild) {
    console.log(
      `[MAIN] Appel sur le serveur : ${message.guild.name} - ${message.guild.id}`
    );
  }
});

const qwerteeRule = new schedule.RecurrenceRule();
qwerteeRule.hour = 23;
qwerteeRule.minute = 15;
qwerteeRule.tz = "Europe/Paris";

// schedule.scheduleJob("*/30 * * * *", () => VMDMain.job(client));

schedule.scheduleJob(qwerteeRule, () => QwerteeMain.job(client));
// schedule.scheduleJob("*/2 * * * *", () => QwerteeMain.job(client));

schedule.scheduleJob("*/30 * * * *", () => EpicBundleMain.job(client));

client.login(process.env.DISCORD_BOT_TOKEN);
